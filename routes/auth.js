var express = require("express");
var fs = require("fs");
var router = express.Router();
const passwordString = fs.readFileSync("password.txt", "utf-8");
const crypto = require("crypto");
const ENC = "bf3c199c2470cb478d907b1e0917c17b";
const IV = "5183666c72eec9e4";
const ALGO = "aes-256-cbc";

// Логин admin Пароль admin

/* POST auth listing. */
router.post("/", function (req, res, next) {
  if (req.body.username === "admin" && req.body.password === decrypt(passwordString)) {
    res.json({ result: "success", token: "1234" });
  } else {
    res.json({ result: "failed" });
  }
});

function decrypt(text) {
  let decipher = crypto.createDecipheriv(ALGO, ENC, IV);
  let decrypted = decipher.update(text, "base64", "utf8");
  return decrypted + decipher.final("utf8");
}

module.exports = router;
