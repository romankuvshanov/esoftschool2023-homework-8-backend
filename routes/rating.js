var express = require('express');
var router = express.Router();

/* GET rating listing. */
router.get('/', function(req, res, next) {
    res.json({rating: RATING_DATA});
});

const RATING_DATA = [
    {
        name: "Александров Игнат Анатолиевич",
        totalAmountOfGames: 24534,
        gamesWonAmount: 9824,
        gamesLostAmount: 1222,
        winsPercentage: 87,
        id: 0,
    },
    {
        name: "Шевченко Рафаил Михайлович",
        totalAmountOfGames: 24534,
        gamesWonAmount: 9824,
        gamesLostAmount: 1222,
        winsPercentage: 87,
        id: 1,
    },
    {
        name: "Мазайло Трофим Артёмович",
        totalAmountOfGames: 24534,
        gamesWonAmount: 9824,
        gamesLostAmount: 1222,
        winsPercentage: 87,
        id: 2,
    },
    {
        name: "Логинов Остин Данилович",
        totalAmountOfGames: 24534,
        gamesWonAmount: 9824,
        gamesLostAmount: 1222,
        winsPercentage: 87,
        id: 3,
    },
    {
        name: "Борисов Йошка Васильевич",
        totalAmountOfGames: 24534,
        gamesWonAmount: 9824,
        gamesLostAmount: 1222,
        winsPercentage: 87,
        id: 4,
    },
    {
        name: "Соловьёв Ждан Михайлович",
        totalAmountOfGames: 24534,
        gamesWonAmount: 9824,
        gamesLostAmount: 1222,
        winsPercentage: 87,
        id: 5,
    },
    {
        name: "Негода Михаил Эдуардович",
        totalAmountOfGames: 24534,
        gamesWonAmount: 9824,
        gamesLostAmount: 1222,
        winsPercentage: 87,
        id: 6,
    },
    {
        name: "Гордеев Шамиль Леонидович",
        totalAmountOfGames: 24534,
        gamesWonAmount: 9824,
        gamesLostAmount: 1222,
        winsPercentage: 87,
        id: 7,
    },
    {
        name: "Многогрешный Павел Виталиевич",
        totalAmountOfGames: 24534,
        gamesWonAmount: 9824,
        gamesLostAmount: 1222,
        winsPercentage: 87,
        id: 8,
    },
    {
        name: "Александров Игнат Анатолиевич",
        totalAmountOfGames: 24534,
        gamesWonAmount: 9824,
        gamesLostAmount: 1222,
        winsPercentage: 87,
        id: 9,
    },
    {
        name: "Волков Эрик Алексеевич",
        totalAmountOfGames: 24534,
        gamesWonAmount: 9824,
        gamesLostAmount: 1222,
        winsPercentage: 87,
        id: 10,
    },
    {
        name: "Кузьмин Ростислав Васильевич",
        totalAmountOfGames: 24534,
        gamesWonAmount: 9824,
        gamesLostAmount: 1222,
        winsPercentage: 87,
        id: 11,
    },
    {
        name: "Стрелков Филипп Борисович",
        totalAmountOfGames: 24534,
        gamesWonAmount: 9824,
        gamesLostAmount: 1222,
        winsPercentage: 87,
        id: 12,
    },
    {
        name: "Галкин Феликс Платонович",
        totalAmountOfGames: 24534,
        gamesWonAmount: 9824,
        gamesLostAmount: 1222,
        winsPercentage: 87,
        id: 13,
    },
];

module.exports = router;
